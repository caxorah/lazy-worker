package logic;

import javax.swing.*;
import java.io.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;
import java.util.Scanner;

public class FileController  {

    final static String FILENAME = "timekeeper.txt";
    final static String ABOUTFILENAME = "About.txt";

    private FileController(){}

    public static LocalDateTime getStartTime(){
        if (isNextDay()){
            overwriteToFile(LocalDateTime.now());
        }
        return getTimeFromFile();
    }

    private static LocalDateTime getTimeFromFile(){
        LocalDateTime stored;
        try {
            File file = new File(FILENAME);
            Scanner input = new Scanner(file);
            stored = LocalDateTime.parse(input.next());
            input.close();
        } catch (FileNotFoundException | DateTimeParseException e){
            overwriteToFile(LocalDateTime.now());
            return LocalDateTime.now();
        }
        return stored;
    }

    private static boolean isNextDay() {
        return LocalDateTime.now().getDayOfYear()!=getTimeFromFile().getDayOfYear();
    }

    public static void overwriteToFile(LocalDateTime newStartTime) {
        try {
            PrintWriter printWriter = new PrintWriter(FILENAME);
            printWriter.println(newStartTime);
            printWriter.close();
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null,"File not found - write.");
        }

    }

    public static BufferedReader getAboutText(){
        InputStream in = FileController.class.getClassLoader().getResourceAsStream(ABOUTFILENAME);
        return new BufferedReader(new InputStreamReader(in));
    }

}
