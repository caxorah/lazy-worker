package logic;

import layout.MainForm;
import layout.StartTimeForm;
import layout.WorkTimeForm;

import javax.swing.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class MenuController {

    public static void showStartTimeChangeWindow (MainForm mainForm){
        new StartTimeForm(mainForm);
    }

    public static void showWorkTimeChangeWindow(MainForm mainForm){
        SwingUtilities.invokeLater(()->new WorkTimeForm(mainForm));
    }


    public static void applyNewWorkTime(WorkTimeForm form){
        int newDuration = 0;
        try {
            newDuration = Integer.parseInt(form.getTxtFieldHoursDuration().getText());
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(form,"Provided value is incorrect!","Error",JOptionPane.ERROR_MESSAGE);
        }
        form.getMainForm().getClockLabel().setWorkdayDuration(newDuration);
        form.getMainForm().setLabelTextAndColor();
        form.dispose();
    }

    public static void applyNewStartTime(StartTimeForm form){
        LocalDateTime newTime = null;
        try {
            newTime = LocalDateTime.of(LocalDate.now(),
                    LocalTime.of(Integer.parseInt(form.getTextFieldHour().getText()),Integer.parseInt(form.getTextFieldMinutes().getText())));
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(form, "Provided value or number format is incorrect!", "Error", JOptionPane.ERROR_MESSAGE);
        }
        FileController.overwriteToFile(newTime);
        form.getMainForm().getClockLabel().setStartTime(newTime);
        form.getMainForm().setLabelTextAndColor();
        form.dispose();
    }



}
