package logic;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ClockLabel {

    private LocalDateTime startTime;
    private LocalDateTime endTime;
    private boolean overtime;
    private int workdayDuration=8;

    public ClockLabel(LocalDateTime startTime){
        this.startTime = startTime;
        this.endTime = startTime.plusHours(workdayDuration);
    }

    private void checkOvertime (){
        this.overtime = this.endTime.isBefore(LocalDateTime.now());
    }

    private LocalDateTime getRemainingTime (){
        int minusHours, minusMinutes;
        this.checkOvertime();
        if (this.overtime){
            minusHours = this.endTime.getHour();
            minusMinutes = this.endTime.getMinute();
            return LocalDateTime.now().minusHours(minusHours).minusMinutes(minusMinutes);
        } else {
            minusHours = LocalDateTime.now().getHour();
            minusMinutes = LocalDateTime.now().getMinute();
            return this.endTime.minusHours(minusHours).minusMinutes(minusMinutes);
        }
    }

    public String getTimeLabel(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");
        String timeDifference = this.getRemainingTime().format(formatter);
        return isOvertime() ? ("-" + timeDifference) : timeDifference;
    }

    public boolean isOvertime (){
        return overtime;
    }

    public void setWorkdayDuration (int workdayDuration){
        this.workdayDuration = workdayDuration;
        this.endTime = startTime.plusHours(this.workdayDuration);
    }

    public void setStartTime (LocalDateTime startTime){
        this.startTime = startTime;
        this.endTime = startTime.plusHours(this.workdayDuration);
    }



}
