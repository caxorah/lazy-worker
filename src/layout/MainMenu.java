package layout;

import logic.MenuController;

import javax.swing.*;
import java.awt.*;

public class MainMenu extends JMenuBar {
    JMenu optionsMenu;
    JMenuItem optionSetTime, optionSetDuration;
    MainForm mainForm;

    public MainMenu (MainForm mainForm){
        optionsMenu = new JMenu("Options");
        optionSetTime = new JMenuItem("Change start time");
        optionSetDuration = new JMenuItem("Change work time");
        this.mainForm = mainForm;

        optionsMenu.add(optionSetTime);
        optionsMenu.add(optionSetDuration);
        optionSetTime.setIcon(new ImageIcon(getClass().getResource("/layout/menuicon1.png")));
        optionSetDuration.setIcon(new ImageIcon(getClass().getResource("/layout/menuicon1.png")));

        optionsMenu.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);

        optionSetTime.addActionListener((click)-> MenuController.showStartTimeChangeWindow(this.mainForm));
        optionSetDuration.addActionListener((click)-> MenuController.showWorkTimeChangeWindow(this.mainForm));

        this.add(optionsMenu);
        this.setBorderPainted(false);
    }

}
