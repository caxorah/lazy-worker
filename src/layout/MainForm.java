package layout;

import logic.ClockLabel;
import logic.FileController;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class MainForm extends JFrame{
    private JPanel myPanel;
    private JLabel myLabel;
    private ClockLabel clockLabel;

    public final static Color TEXT_COLOR_RED = new Color(200, 24, 16);
    public final static Color TEXT_COLOR_GREY = new Color(60, 60, 60);

    public MainForm () {
        add(myPanel);
        setTitle("LazyWorker");
        setSize(270,170);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setIconImage(loadImage("icon.png"));
        setLocation(1100,20);

        setJMenuBar(new MainMenu(this));
        clockLabel = new ClockLabel(FileController.getStartTime());
        setLabelTextAndColor();

        Timer timer = new Timer(60000, minuteChangeEvent -> setLabelTextAndColor());
        timer.start();
    }

    public BufferedImage loadImage (String fileName){
        BufferedImage image = null;
        try {
            image = ImageIO.read(getClass().getResourceAsStream(fileName));
        } catch (IOException e) {}//no need for any action
        return image;
    }

    public ClockLabel getClockLabel (){
        return clockLabel;
    }

    public void setLabelTextAndColor (){
        this.myLabel.setText(clockLabel.getTimeLabel());
        Color textcolor = clockLabel.isOvertime() ? TEXT_COLOR_RED : TEXT_COLOR_GREY;
        this.myLabel.setForeground(textcolor);
    }
}
