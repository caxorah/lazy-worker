package layout;

import logic.MenuController;
import javax.swing.*;

public class StartTimeForm extends JFrame {
    private JLabel startTimeInstructionLabel;
    private JTextField textFieldHour;
    private JTextField textFieldMinutes;
    private JButton cancelButton;
    private JButton confirmButton;
    private JLabel labelMM;
    private JLabel labelHH;
    private JLabel labelColon;
    private JPanel stPanel;
    private MainForm mainForm;

    public StartTimeForm (MainForm mf){
        this.mainForm = mf;
        add(stPanel);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300,200);
        setTitle("Set start time");
        setIconImage(mainForm.loadImage("icon.png"));
        setLocation(500,80);
        setVisible(true);

        cancelButton.addActionListener((click)-> this.dispose());
        confirmButton.addActionListener((click)-> MenuController.applyNewStartTime(this));
    }

    public JTextField getTextFieldHour (){
        return textFieldHour;
    }

    public JTextField getTextFieldMinutes (){
        return textFieldMinutes;
    }

    public MainForm getMainForm (){
        return mainForm;
    }
}
