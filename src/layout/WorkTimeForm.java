package layout;

import logic.MenuController;

import javax.swing.*;

public class WorkTimeForm extends JFrame {
    private JPanel sdPanel;
    private JButton applyButton;
    private JTextField txtFieldHoursDuration;
    private JButton cancelButton;
    private JLabel setWorkTimeLabel;
    private MainForm mainForm;

    public WorkTimeForm (MainForm mainForm){
        this.mainForm = mainForm;
        add(sdPanel);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setSize(300,150);
        setLocation(500,90);
        setTitle("Change work time");
        setIconImage(mainForm.loadImage("icon.png"));
        setVisible(true);

        cancelButton.addActionListener((click)->this.dispose());
        applyButton.addActionListener((click)-> MenuController.applyNewWorkTime(this));
    }

    public JTextField getTxtFieldHoursDuration (){
        return txtFieldHoursDuration;
    }

    public MainForm getMainForm (){
        return mainForm;
    }
}
